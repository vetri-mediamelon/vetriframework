Pod::Spec.new do |spec|

  spec.name         = "VetriFramework"
  spec.version      = "1.0.0"
  spec.summary      = "Sample VetriFramework."
  spec.description  = "This is MMSmartStreaming Sample CocoaPods"
  spec.homepage     = "https://vetri-mediamelon@bitbucket.org/vetri-mediamelon/vetriframework"
  spec.license      = "MIT"
  spec.author       = { "Vetri" => "vetri@mediamelon.com" }
  spec.platform     = :ios, "11.0"
  spec.source       = { :git => "https://vetri-mediamelon@bitbucket.org/vetri-mediamelon/vetriframework.git", :tag => "1.0.0" }
  spec.source_files = "MMSmartStreaming/**/*"
  spec.swift_versions = "4.0"

end
